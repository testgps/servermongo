// routes/note.js
const express = require('express');
const router = express.Router();
const Note = require('../models/note');
const cors = require('cors');
const app = express();

app.use(cors());

// Resto de tu configuración...

// Ruta para crear una nueva nota
// Ruta para crear una nueva nota
router.post('/note', async (req, res) => {
	try {
		console.log('Nueva nota recibida:', req.body);
		const newNote = new Note(req.body);
		const savedNote = await newNote.save();
		console.log('Nota guardada:', savedNote);
		res.json(savedNote);
	} catch (error) {
		const errorMessage = 'Error al crear la nota';
		console.error(errorMessage, error);
		res.status(500).json({ error: errorMessage });
	}
});

// Ruta para obtener todas las notas http://localhost:3000/api/note
router.get('/note', async (req, res) => {
	try {
		const notes = await Note.find();
		if (notes.length === 0) {
			return res.status(404).json({ error: 'No se encontraron notas' });
		}
		res.json(notes);
	} catch (error) {
		console.error('Error al obtener las notas:', error);
		res.status(500).json({ error: 'Error al obtener las notas' });
	}
});

// Ruta para actualizar una nota
router.put('/note/:id', (req, res) => {
	Note.findByIdAndUpdate(req.params.id, req.body, { new: true })
		.then((data) => res.json(data))
		.catch((error) => res.json({ message: error }));
});

// Ruta para eliminar una nota
router.delete('/note/:id', async (req, res) => {
	try {
		const noteId = req.params.id;

		if (!noteId || noteId === 'undefined') {
			return res
				.status(400)
				.json({ error: 'ID de nota no proporcionado o no válido' });
		}

		const deletedNote = await Note.findByIdAndRemove(noteId);

		if (!deletedNote) {
			return res.status(404).json({ error: 'Nota no encontrada' });
		}

		console.log('Nota eliminada correctamente:', deletedNote); // Agregado
		res.json({ message: 'Nota eliminada correctamente', deletedNote });
	} catch (error) {
		console.error('Error al eliminar la nota:', error);
		res.status(500).json({ error: 'Error al eliminar la nota' });
	}
});

module.exports = router;
